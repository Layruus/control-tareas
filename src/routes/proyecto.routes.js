const express = require('express');
const router = express.Router();

const Proyecto = require('../models/proyecto')

router.get('/', async (req, res) => {
    const proyectos = await Proyecto.find();
    res.json(proyectos);
});

router.get('/:id', async (req, res) => {
    const proyecto = await Proyecto.findById(req.params.id);
    res.json(proyecto);
});

router.post('/', async (req, res) =>{
    const {nombre, descripcion, usuario, tareas} = req.body;
    const proyecto = new Proyecto({nombre, descripcion, usuario, tareas});
    await proyecto.save(),
    res.json({status: 'Proyecto almacenado'});
});

router.put('/:id', async (req, res) =>{
    const {nombre, descripcion, usuario, tareas} =req.body;
    const newProyecto = {nombre, descripcion, usuario, tareas};
    await Proyecto.findByIdAndUpdate(req.params.id, newProyecto);
    console.log(req.params.id);
    res.json({status: 'Proyecto actualizado'});
});

router.delete('/:id', async (req,res) => {
    await Proyecto.findByIdAndRemove(req.params.id);
    res.json({status: 'Proyecto eliminado'});
});

module.exports = router;