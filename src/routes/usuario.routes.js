const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const Usuario = require('../models/usuario');
const verifyToken = require('../middleware/verifyToken');
const config = require('../config');


router.post('/signin', async (req, res) => {
    const usuario = await Usuario.findOne({email: req.body.email})
    if(!usuario) {
        return res.status(404).send("El email no existe")
    }
    const validPassword = await bcrypt.compare(req.body.password, usuario.password);
    if (!validPassword) {
        return res.status(401).send({auth: false, token: null});
    }
    const token = jwt.sign({id: usuario._id}, config.secret, {
        expiresIn: 60 * 60 * 24
    });
    res.status(200).json({auth: true, token});
});

router.get('/logout', function(req, res) {
    res.status(200).send({ auth: false, token: null });
});

router.get('/me', verifyToken, async (req, res) => {
    // res.status(200).send(decoded);
    // Search the Info base on the ID
    // const user = await User.findById(decoded.id, { password: 0});
    const usuario = await Usuario.findById(req.usuarioId, { password: 0});
    if (!usuario) {
        return res.status(404).send("No user found.");
    }
    res.status(200).json(usuario);
});

router.get('/', async (req, res) => {
    const usuarios = await Usuario.find();
    res.json(usuarios);
});

router.get('/:id', verifyToken, async (req, res) => {
    const usuario = await Usuario.findById(req.params.id);
    res.json(usuario);
});

router.post('/', async (req, res) =>{
    const {email, password, nombres, perfil, imagen} = req.body;
    const usuario = new Usuario({email, password, nombres, perfil, imagen});
    const salt = await bcrypt.genSalt(10);
    usuario.password = await bcrypt.hash(password, salt);
    await usuario.save(),
    res.json({status: 'Usuario almacenado'});
});

router.put('/:id', async (req, res) =>{
    const {email, password, nombres, perfil, imagen} =req.body;
    const newUsuario = {email, password, nombres, perfil, imagen};
    const salt = await bcrypt.genSalt(10);
    newUsuario.password = await bcrypt.hash(password, salt);
    await Usuario.findByIdAndUpdate(req.params.id, newUsuario);
    console.log(newUsuario);
    res.json({status: 'Usuario actualizado'});
});

router.delete('/:id', async (req,res) => {
    await Usuario.findByIdAndRemove(req.params.id);
    res.json({status: 'Usuario eliminado'});
});

module.exports = router;