const express = require('express');
const router = express.Router();

const Comentario = require('../models/comentario')

router.get('/', async (req, res) => {
    const comentarios = await Comentario.find();
    res.json(comentarios);
});

router.get('/:id', async (req, res) => {
    const comentario = await Comentario.findById(req.params.id);
    res.json(comentario);
});

router.post('/', async (req, res) =>{
    const {comentario, adjunto, usuario, hilo} = req.body;
    const comentarioO = new Comentario({comentario, adjunto, usuario, hilo});
    await comentarioO.save(),
    res.json({status: 'Comentario almacenado'});
});

router.put('/:id', async (req, res) =>{
    const {comentario, adjunto, usuario, hilo} =req.body;
    const newComentario = {comentario, adjunto, usuario, hilo};
    await Comentario.findByIdAndUpdate(req.params.id, newComentario);
    console.log(req.params.id);
    res.json({status: 'Comentario actualizado'});
});

router.delete('/:id', async (req,res) => {
    await Comentario.findByIdAndRemove(req.params.id);
    res.json({status: 'Comentario eliminado'});
});

module.exports = router;