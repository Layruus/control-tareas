const mongoose = require('mongoose');
const {Schema} = mongoose;

const ComentarioSchema = new Schema({
    comentario: {type: String, required: true},
    adjunto: {type: String, required: true},
    usuario: {
        type: Schema.Types.ObjectId,
        ref: 'usuario'
    },
    hilo: {
        type: Schema.Types.ObjectId,
        ref: 'comentario'
    }
},{timestamps: true});

module.exports = mongoose.model('Comentario', ComentarioSchema);