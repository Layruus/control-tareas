import React, { Component } from "React";

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: ""
        };
        this.login = this.login.bind(this);
        this.actEstado = this.actEstado.bind(this);
    }

    login(e) {
        fetch("/api/usuario/signin", {
            method: "POST",
            body: JSON.stringify(this.state),
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(data => {
                if(data.auth) {
                    console.log(data)
                    this.props.log(data)
                }else {
                    alert('Usuario o contraseña incorrecta');
                }
            })
            .catch(err => console.log(err));
        e.preventDefault();
    }

    actEstado(e) {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        });
    }

    componentDidMount() {
        const data = {
            email: "",
            password: ""
        };
        this.setState(data);
    }

    render() {
        return (
            <div className="container-fluid login-container bg-white">
                <div className="row bg-white">
                    <div className="col login-form-1">
                        <h3>Control de Tareas</h3>
                        <form onSubmit={this.login}>
                            <div className="form-group">
                                <input
                                    name="email"
                                    type="text"
                                    placeholder="Email"
                                    className="form-control"
                                    onChange={this.actEstado}
                                    value={this.state.email}
                                />
                            </div>
                            <div className="form-group">
                                <input
                                    name="password"
                                    type="password"
                                    placeholder="Password"
                                    className="form-control"
                                    onChange={this.actEstado}
                                    value={this.state.password}
                                />
                            </div>
                            <div className="form-group">
                                <input type="submit" className="btnSubmit" value="Login" />
                            </div>
                            <div className="form-group">
                                <a href="#" className="ForgetPwd">
                                    ¿Olvidaste tu contraseña?
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;
