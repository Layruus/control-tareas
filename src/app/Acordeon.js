import React, { Component } from "React";

import Proyecto from "./proyecto";

class Acordeon extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nombres: "",
            perfil: ""
        };
    }

    render() {
        return (
            <div className="card">
                <div className="card-header" role="tab" id="headingOne1">
                    <a
                        data-toggle="collapse"
                        data-parent="#accordionEx"
                        href="#collapseOne1"
                        aria-expanded="true"
                        aria-controls="collapseOne1"
                    >
                        <h5 className="mb-0">
                            UI/X Design Proyect
                            <i className="fas fa-angle-down rotate-icon"></i>
                        </h5>
                    </a>
                </div>
                <div
                    id="collapseOne1"
                    className="collapse show"
                    role="tabpanel"
                    aria-labelledby="headingOne1"
                    data-parent="#accordionEx"
                >
                    <div className="card-body">
                        <Proyecto />
                        <Proyecto />
                        <Proyecto />
                        <Proyecto />
                    </div>
                </div>
            </div>
        );
    }
}
export default Acordeon;
