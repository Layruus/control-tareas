import React, { Component } from "React";

import Login from "./login";
import Principal from "./principal";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            auth: false,
            token: ""
        };
        this.handleLoginClick = this.handleLoginClick.bind(this);
    }

    componentDidMount() {
        this.setState({
            auth: false,
            token: ""
        });
    }

    handleLoginClick(data) {
        this.setState({
            auth: data.auth,
            token: data.token
        });
    }

    render() {
        let ventana;
        if (this.state.auth) {
            ventana = (<Principal token={this.state.token}/>);
        } else {
            ventana = (<Login log={this.handleLoginClick} />);
        }
        return  ventana;
    }
}
export default App;
