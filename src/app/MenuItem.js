import React, { Component } from "React";

class MenuItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: {
                isSelect: false,
                list_item: "list-group-item list-group-item-action bg-white"
            }
        };
        this.toggleItem = this.toggleItem.bind(this);
    }

    componentDidMount() {
        this.setState({
            selected: {
                isSelect: false,
                list_item: "list-group-item list-group-item-action bg-white",
            }
        });
    }

    toggleItem() {
        if (this.state.selected.isSelect) {
            this.setState({
                selected: {
                    isSelect: false,
                    list_item: "list-group-item list-group-item-action bg-white"
                }
            });
        } else {
            this.setState({
                selected: {
                    isSelect: true,
                    list_item: "list-group-item list-group-item-action bg-white select"
                }
            });
        }
    }

    render() {
        return (
            <a href="#" className={this.state.selected.list_item} onClick={this.toggleItem}>
                <i className={"fa " + this.props.icon + " fa-lg"}></i>
                <span className={this.props.hide}>{this.props.name}</span>
            </a>
        );
    }
}
export default MenuItem;
