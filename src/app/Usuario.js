import React, { Component } from "React";

class AreaTrabajo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nombres: "",
            perfil: ""
        };
        this.getUsuario();
    }

    componentDidMount() {
        const data = {
            nombres: "",
            perfil: ""
        };
        this.setState(data);
    }
    
    getUsuario() {
        console.log(this.props.token);
        fetch('/api/usuario/me', {
            method: "GET",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                "x-access-token": this.props.token
            }
        })
        .then(res => res.json())
        .then(data =>{
            console.log(data);
            this.setState({
                nombres: data.nombres,
                perfil: data.perfil
            });
        });
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-white border-left">
                <div
                    className="imagen"
                    style={{ backgroundImage: "url(" + "images/perfil3.jpg" + ")" }}
                ></div>
                <div className="user">
                    <span>{this.state.nombres}</span>
                    <span>{this.state.perfil}</span>
                </div>
            </nav>
        );
    }
}
export default AreaTrabajo;
